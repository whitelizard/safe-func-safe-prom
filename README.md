# safe-func-safe-prom

---

**This module quickly turned into [tri-fp](https://www.npmjs.com/package/tri-fp) which is the active library for this functionality. You can:**

```javascript
import { tri as safe, triP as safeP } from "tri-fp";
```

**...if you like the names in this module.**

---

- Make both functions and promises more functional by removing the need for try-catch.
- Handle application errors and native exceptions differently.

Wrap a _function_ in `safe`, or a _promise_ in `safeP`, to transform all outcomes **except native exceptions** into an _error-first pair_. Don't use `try-catch` or `.catch`.

```javascript
import { safe, safeP } from "safe-func-safe-prom";

// Sync
const [error, result] = safe(myFunction)(arg1, arg2);

// Async, direct promise
const [error, result] = await safeP(myPromise);

// Async, promise returning function
const [error, result] = await safeP(myPromiseReturningFunc)(arg1, arg2);

// Native errors/exceptions will still throw. Don't catch these; Find your bug.
```

## Motivation

- [https://dev.to/craigmichaelmartin/the-problem-with-promises-in-javascript-5h46](https://dev.to/craigmichaelmartin/the-problem-with-promises-in-javascript-5h46)
- [https://medium.com/better-programming/js-reliable-fdea261012ee](https://medium.com/better-programming/js-reliable-fdea261012ee)
- [https://medium.com/@gunar/async-control-flow-without-exceptions-nor-monads-b19af2acc553](https://medium.com/@gunar/async-control-flow-without-exceptions-nor-monads-b19af2acc553)

## Inspired by / Thanks to

- [`fAwait`](https://github.com/craigmichaelmartin/fawait)
- [`saferr`](https://github.com/suzdalnitski/saferr)
