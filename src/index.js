// native errors
const exceptions = [
  EvalError,
  RangeError,
  ReferenceError,
  SyntaxError,
  TypeError,
  URIError,
].filter((ex) => typeof ex === "function");

const checkException = (err) => {
  exceptions.forEach((ex) => {
    if (err instanceof ex) throw err;
  });
};

export const safeP = (promise) =>
  typeof promise === "function"
    ? (...args) => safeP(promise(...args))
    : promise.then(
        (data) => [undefined, data],
        (err) => {
          checkException(err);
          return [err];
        }
      );

export const safe = (func) => (...args) => {
  try {
    const result = func(...args);
    return [undefined, result];
  } catch (err) {
    checkException(err);
    return [err];
  }
};
