import { test } from "tap";
import { safe, safeP } from "../src";

test("safe func should return result in tuple", (t) => {
  const func = (x) => x * 2;
  const [err, res] = safe(func)(3);
  t.equals(res, 6);
  t.equals(err, undefined);
  t.end();
});

test("safe func should not throw custom errors", (t) => {
  const func = () => {
    throw new Error("message");
  };
  const [err, res] = safe(func)();
  t.equals(res, undefined);
  t.equals(err.message, "message");
  t.end();
});

test("safe func should throw native exceptions", (t) => {
  const func = (a) => a.b;
  t.throws(safe(func));
  t.end();
});

test("safeP func should return result in tuple", async (t) => {
  const [err, res] = await safeP(Promise.resolve(5));
  t.equals(res, 5);
  t.equals(err, undefined);
});

test("safeP func should accept promise returning function", async (t) => {
  const getPromise = (x) => Promise.resolve(x);
  const [err, res] = await safeP(getPromise)(5);
  t.equals(res, 5);
  t.equals(err, undefined);
});

test("safeP func should not throw custom errors", async (t) => {
  const [err, res] = await safeP(Promise.reject(new Error("message")));
  t.equals(res, undefined);
  t.equals(err.message, "message");
  const [err2, res2] = await safeP(
    new Promise(() => {
      throw new Error("message");
    })
  );
  t.equals(res2, undefined);
  t.equals(err2.message, "message");
});
